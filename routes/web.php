<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    Route::name('dashboard')->get('/', 'HomeController@index');
    // Users
    Route::resource('users', 'UsersController');
    //
    Route::resource('locations', 'LocationsController');
    // Reset Password Requests
    Route::resource('resets', 'ResetsController', ['only' => ['index', 'edit']]);
    Route::delete('resets/{id}', 'ResetsController@destroy')->name('resets.destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
