<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Activities
Route::resource('activities', 'ActivitiesController', ['only' => ['index']]);
//login
Route::name('login')->post('login', 'LoginController@login');
//Days
Route::resource('days', 'DaysController', ['only' => ['store', 'update']]);
// user type check
Route::name('type_check')->post('type_check', 'TypeCheckController@store');
// location
Route::name('locations')->get('locations', 'LocationsController@index');
//location trackers
Route::resource('location_trackers', 'LocationTrackersController');
//Office Activities
Route::resource('office_activities', 'OfficeActivitiesController');
Route::name('office_activities.complete')->post('office_activities/complete', 'OfficeActivitiesController@complete');

Route::name('office_activities.break')->post('office_activities/break', 'OfficeActivitiesController@break');
Route::name('office_activities.end_break')
    ->post('office_activities/end_break/{office_activity}', 'OfficeActivitiesController@endBreak');
Route::name('office_activities.upload_attachment')
    ->post('upload_attachment/{office_activity}', 'OfficeActivitiesController@uploadAttachment');
Route::name('office_activities.history')->get('office_activities_history', 'OfficeActivitiesController@history');

// Travel Details
Route::resource('travel_details', 'TravelDetailsController');
// clients
Route::name('clients')->get('clients', 'ClientsController@index');
Route::name('managers')->get('managers', 'UsersController@managers');
Route::name('sales')->get('sales', 'UsersController@sales');
// visits
Route::resource('visits', 'VisitsController');
// Reset Password
Route::resource('resets', 'ResetsController', ['only' => 'store']);
//Reports
Route::get('reports', 'ReportsController@index')->name('reports');
Route::get('reports/export', 'ReportsController@export')->name('reports.export');
//clients requests
Route::resource('requests', 'ClientsRequestsController', ['only' => ['store', 'index', 'update']]);
// visit Activities
Route::name('visit_activities')->post('visit_activities', 'VisitActivitiesController@store');
// contacts
Route::apiResource('contacts', 'ContactsController');
Route::name('user_contacts')->get('user_contacts', 'ContactsController@userContacts');
