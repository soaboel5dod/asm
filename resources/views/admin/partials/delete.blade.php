<a class="btn btn-danger btn-xs" href="#" data-toggle="modal" data-target="#delete-{{ $id }}">
    <i class="fa fa-trash"></i>
    Delete
</a>
<div class="modal modal-danger fade" id="delete-{{ $id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete {{ $title }} ?</h4>
            </div>
            {!! Form::open(['method' => 'DELETE', 'url' => $route]) !!}
            <div class="modal-body">
                <p>{{ $body }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline">Delete</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>