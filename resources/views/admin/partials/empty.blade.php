<div class="empty-collection">
    <i class="fa fa-times fa-4x"></i>
    <h1>{{ $message }}</h1>
</div>