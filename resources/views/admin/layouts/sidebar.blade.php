<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/build/img/avatar.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::check() ? Auth::user()->name : 'Dev' }}</p>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ isRouteActive('admin.dashboard') ? ' active' : '' }}"><a href="{{ route('admin.dashboard') }}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview{{ isRouteActive('admin.users.*') ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-users"></i><span>Users</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.users.index') }}">All Users</a></li>
                    <li><a href="{{ route('admin.users.create') }}">Create User</a></li>
                </ul>
            </li>
            <li class="treeview{{ isRouteActive('admin.locations.*') ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-users"></i><span>Locations</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.locations.index') }}">All Locations</a></li>
                    <li><a href="{{ route('admin.locations.create') }}">Create Location</a></li>
                </ul>
            </li>
            <li class="{{ isRouteActive('admin.customers.index') ? ' active' : '' }}"><a href="{{ route('admin.resets.index') }}"> <i class="fa fa-users"></i> <span>Reset Passwords</span></a></li>
            {{--<li class="treeview{{ isRouteActive('admin.products.*') ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-cubes"></i><span>Products</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.products.index') }}">Products</a></li>
                    <li><a href="{{ route('admin.components.index') }}">Components</a></li>
                    <li><a href="{{ route('admin.modifiers.index') }}">Modifiers</a></li>
                </ul>
            </li>
            <li class="{{ isRouteActive('admin.customers.index') ? ' active' : '' }}"><a href="{{ route('admin.customers.index') }}"> <i class="fa fa-users"></i> <span>Customers</span></a></li>
            <li class="{{ isRouteActive('admin.daily_summary') ? ' active' : '' }}"><a href="{{ route('admin.daily_summary') }}"> <i class="fa fa-users"></i> <span>Daily Summery</span></a></li>
            <li><a href="#"><i class="fa fa-info"></i> <span>Settings</span></a></li>--}}
            <li>
                <a href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> <span>Sign out</span></a>
            </li>
        </ul>
    </section>
</aside>
