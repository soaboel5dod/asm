@extends('admin.layouts.app')
@section('page.title', 'Reset Requests')
@section('page.description')
    {{--<a class="btn btn-success btn-xs" href="{{ route('admin.resets.create') }}"><i class="fa fa-plus"></i> New</a>--}}
@stop
@section('page.breadcrumb')
    <li class="active">Reset Requests</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Reset Requests</h3>
            <div class="box-tools">
                {{--<a href="{{ route('admin.areas.export') }}" class="btn btn-box-tool"><i class="fa fa-download"></i></a>--}}
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        @if(count($resets))
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($resets as $reset)
                        <tr>
                            <td>{{ $reset->email }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('admin.resets.edit', $reset->email) }}">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                @include('admin.partials.delete', [
                                    'id' => $reset->id,
                                    'route' => route('admin.resets.destroy', $reset->id),
                                    'title' => $reset->email,
                                    'body' => 'You cannot undo this action.'
                                ])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $resets->render() !!}
            </div>
    </div>
    @else
        @include('admin.partials.empty', ['message' => "No Reset Requests Received yet."])
    @endif
@stop