@extends('admin.layouts.app')

@section('page.title', 'Edit User')
@section('page.description', 'update an user information')
@section('page.breadcrumb')
    <li><a href="{{ route('admin.users.index') }}">Users</a></li>
    <li class="active">Edit User</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit User</h3>
        </div>
        {!! Form::model($user, ['method' => 'PATCH', 'url' => route('admin.users.update', $user->id)]) !!}
        @include('admin.users._form')
        {!! Form::close() !!}
    </div>
@stop