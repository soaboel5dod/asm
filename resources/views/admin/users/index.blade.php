@extends('admin.layouts.app')
@section('page.title', 'Users')
@section('page.description')
    <a class="btn btn-success btn-xs" href="{{ route('admin.users.create') }}"><i class="fa fa-plus"></i> New</a>
@stop
@section('page.breadcrumb')
    <li class="active">Users</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Users</h3>
            <div class="box-tools">
                {{--<a href="{{ route('admin.areas.export') }}" class="btn btn-box-tool"><i class="fa fa-download"></i></a>--}}
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        @if(count($users))
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Manager</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type }}</td>
                            <td>{{ $user->manager? $user->manager->name : '' }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('admin.users.edit', $user->id) }}">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                @include('admin.partials.delete', [
                                    'id' => $user->id,
                                    'route' => route('admin.users.destroy', $user->id),
                                    'title' => $user->name,
                                    'body' => 'You cannot undo this action.'
                                ])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $users->render() !!}
            </div>
    </div>
    @else
        @include('admin.partials.empty', ['message' => "You didn't create any Users yet."])
    @endif
@stop