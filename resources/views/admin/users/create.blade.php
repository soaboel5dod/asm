@extends('admin.layouts.app')

@section('page.title', 'Users')
@section('page.description', 'Create New User')
@section('page.breadcrumb')
    <li><a href="{{ route('admin.users.index') }}">Users</a></li>
    <li class="active">Add New</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">New User</h3>
        </div>
        {!! Form::open(['url' => route('admin.users.store'),'files'=>'true']) !!}
        @include('admin.users._form')
        {!! Form::close() !!}
    </div>
@stop