<div class="box-body">
    <input type="hidden" name="id" value="{{ isset($user) ? $user->id : null }}">
    <div class="form-group">
        {!! Form::label('manager_id', 'Manager') !!}
        {!! Form::select('manager_id', $managers, null, ['class' => 'form-control','placeholder' => 'select Manager']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('type', 'User Type') !!}
        {!! Form::select('type', ['sales' => 'sales', 'management'=> 'management'], null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="box-footer">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
