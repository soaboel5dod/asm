@extends('admin.layouts.app')
@section('page.description', 'Admin Dashboard')

@section('content')
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $resets }}</h3>

                <p>Reset Password Requests</p>
            </div>
            <div class="icon">
                <i class="fa fa-envelope-o"></i>
            </div>
            <a href="{{ route('admin.resets.index') }}" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    {{--<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! $customersChart->render() !!}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! $ordersChart->render() !!}
        </div>
    </div>--}}
@stop
