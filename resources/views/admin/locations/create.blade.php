@extends('admin.layouts.app')

@section('page.title', 'Locations')
@section('page.description', 'Create New Location')
@section('page.breadcrumb')
    <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
    <li class="active">Add New</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">New location</h3>
        </div>
        {!! Form::open(['url' => route('admin.locations.store')]) !!}
        @include('admin.locations._form')
        {!! Form::close() !!}
    </div>
@stop