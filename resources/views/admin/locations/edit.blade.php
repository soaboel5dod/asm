@extends('admin.layouts.app')

@section('page.title', 'Edit Location')
@section('page.description', 'update an location information')
@section('page.breadcrumb')
    <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
    <li class="active">Edit location</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Location</h3>
        </div>
        {!! Form::model($location, ['method' => 'PATCH', 'url' => route('admin.locations.update', $location->id)]) !!}
        @include('admin.locations._form')
        {!! Form::close() !!}
    </div>
@stop