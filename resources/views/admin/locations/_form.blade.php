<div class="box-body">
    <input type="hidden" name="id" value="{{ isset($location) ? $location->id : null }}">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('latitude', 'latitude') !!}
        {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('longitude', 'longitude') !!}
        {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::hidden('type', 'office', ['class' => 'form-control']) !!}
</div>

<div class="box-footer">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
