@extends('admin.layouts.app')
@section('page.title', 'Locations')
@section('page.description')
    <a class="btn btn-success btn-xs" href="{{ route('admin.locations.create') }}"><i class="fa fa-plus"></i> New</a>
@stop
@section('page.breadcrumb')
    <li class="active">Locations</li>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Locations</h3>
            <div class="box-tools">
                {{--<a href="{{ route('admin.areas.export') }}" class="btn btn-box-tool"><i class="fa fa-download"></i></a>--}}
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        @if(count($locations))
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>latitude</th>
                        <th>longitude</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($locations as $location)
                        <tr>
                            <td>{{ $location->name }}</td>
                            <td>{{ $location->latitude }}</td>
                            <td>{{ $location->longitude }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('admin.locations.edit', $location->id) }}">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                @if($location->id != 1)
                                    @include('admin.partials.delete', [
                                        'id' => $location->id,
                                        'route' => route('admin.locations.destroy', $location->id),
                                        'title' => $location->name,
                                        'body' => 'You cannot undo this action.'
                                    ])
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $locations->render() !!}
            </div>
    </div>
    @else
        @include('admin.partials.empty', ['message' => "You didn't create any Users yet."])
    @endif
@stop