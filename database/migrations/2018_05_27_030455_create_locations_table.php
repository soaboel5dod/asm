<?php

use App\Location;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', ['office', 'visit', 'travel', 'break']);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->timestamps();
        });

        Location::create([
            'name' => 'office',
            'latitude' => '31.22',
            'type' => 'office',
            'longitude' => '30.33',
        ]);
        Location::create([
            'name' => 'visit',
            'type' => 'visit',
        ]);
        Location::create([
            'name' => 'travel',
            'type' => 'travel',
        ]);
        Location::create([
            'name' => 'break',
            'type' => 'break',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
