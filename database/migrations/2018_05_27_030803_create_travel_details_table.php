<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('location_tracker_id');
            $table->enum('type', ['local', 'domestic', 'international']);
            $table->string('start_location');
            $table->string('destination_location');
            $table->timestamp('start_time');
            $table->timestamp('end_time')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();

            $table->foreign('location_tracker_id')
                ->references('id')
                ->on('location_trackers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_details');
    }
}
