<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('manager_id')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('type', ['sales', 'management']);
            $table->boolean('admin')->default(false);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });

        User::create([
            'name' => 'Admin',
            'email' => 'admin@asm.com',
            'password' => bcrypt('asmadmin'),
            'type' => 'management',
            'admin' => true
        ]);
        User::create([
            'name' => 'management',
            'email' => 'management@asm.com',
            'password' => bcrypt('managementadmin'),
            'type' => 'management'
        ]);

        User::create([
            'name' => 'sales',
            'email' => 'sales@asm.com',
            'password' => bcrypt('salesadmin'),
            'type' => 'sales',
            'manager_id' => 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
