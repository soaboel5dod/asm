<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTracker extends Model
{
    protected $dates = [
        'start', 'checkout'
    ];
    protected $fillable = ['user_id', 'location_id', 'day_id', 'start', 'checkout', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function day()
    {
        return $this->belongsTo(Day::class);
    }

    public function visits()
    {
        return $this->hasMany(Visit::class);
    }

    public function officeActivities()
    {
        return $this->hasMany(OfficeActivity::class);
    }
}
