<?php

namespace App\Traits;

use App\Photo;

trait Photoable
{
    /**
     * Boots the trait for the eloquent models.
     */
    public static function bootPhotoable()
    {
        // Delete Old Photos.
        static::deleting(function ($model) {
            $model->deleteImages();
        });
    }

    /**
     * Gets the model photos.
     *
     * @return mixed
     */
    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable')->where('featured', false);
    }

    /**
     * @return mixed
     */
    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable')->where('featured', true);
    }

    /**
     * Adds an image or multiple images to the model.
     *
     * @param $paths
     * @param bool $featured
     * @return static
     */
    public function addImage($paths, $featured = false)
    {
        $images = collect(is_array($paths) ? $paths : [$paths])->map(function ($path) use (&$featured) {
            $photo = $this->photos()->create(['path' => $path, 'featured' => $featured]);
            $featured = false;
            return $photo;
        });

        return $images->count() === 1 ? $images[0] : $images;
    }

    /**
     * Replaces the current image with a new one.
     * @param $paths
     * @param bool $featured
     */
    public function replaceImage($paths, $featured = false)
    {
        if ($featured && $this->photo) {
            $this->photo->delete();
        } else {
            $this->photos->each(function ($photo) {
                $photo->delete();
            });
        }
        $this->addImage($paths, $featured);
    }

    /**
     * @return mixed
     */
    public function getThumbnailAttribute()
    {
        return $this->photo->getThumbnail();
    }

    /**
     * Deletes all images.
     */
    public function deleteImages()
    {
        $this->photos->each(function ($photo) {
            $photo->delete();
        });
    }

    /**
     * @return null|string
     */
    public function getImageAttribute()
    {
        $photo = $this->photo;
        if (! $photo) {
            return null;
        }

        return asset('storage/' . $photo->path);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getImagesAttribute()
    {
        $photos = $this->photos;
        if (! $photos || ! $photos->count()) {
            return collect([]);
        }

        return $photos->map(function ($photo) {
            return $photo;
        });
    }
}
