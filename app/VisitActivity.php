<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitActivity extends Model
{
    protected $fillable = ['activity_id', 'visit_id', 'note', 'contact_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visit()
    {
        return $this->belongsTo(Visit::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
