<?php

namespace App;

use App\Traits\Photoable;
use Illuminate\Database\Eloquent\Model;

class OfficeActivity extends Model
{
    use Photoable;
    protected $dates = [
        'start', 'end'
    ];
    protected $fillable = ['activity_id', 'location_tracker_id', 'start', 'end', 'note', 'break'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function locationTracker()
    {
        return $this->belongsTo(LocationTracker::class);
    }
}
