<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'manager_id' => 'required_if:type,sales',
            'email' => 'required|string|email|max:255|unique:users,email,'.$this->request->get('id'),
            'password' => ($this->method() === 'POST' ? 'required|string|min:6' : ''),
            'type' => 'required|in:sales,management'
        ];
    }
}
