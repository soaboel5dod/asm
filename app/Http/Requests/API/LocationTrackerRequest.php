<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;
use Illuminate\Http\Request;

class LocationTrackerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'user_id' => 'required|exists:users,id',
            'location_id' => 'required|exists:locations,id',
            'day_id' => 'required|exists:days,id',
            'lat' => 'required_if:location_id,1',
            'long' => 'required_if:location_id,1'
        ];
    }
}
