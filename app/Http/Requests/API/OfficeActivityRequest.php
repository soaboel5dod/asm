<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;
use Illuminate\Http\Request;

class OfficeActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'activity_id' => 'required|exists:activities,id',
            'location_tracker_id' => 'required|exists:location_trackers,id',
            'start' => 'required'
        ];
    }
}
