<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;
use Illuminate\Http\Request;

class TravelDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [

            'location_tracker_id' => ($this->method() === 'POST' ? 'required|exists:location_trackers,id|' : ''),
            'type' => ($this->method() === 'POST' ? 'required|' : ''),
            'start_location' => 'required',
            'destination_location' => 'required',
            'start_time' => 'required',
            'end_time' => 'required_unless:type,local'
        ];
    }
}
