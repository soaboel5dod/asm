<?php

namespace App\Http\Middleware;

use Closure;

class GrantAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()) {
            return redirect()->route('login');
        }
        if (! $request->user()->admin) {
            return redirect()->route('login')->withErrors('Permission Denied');
        }
        return $next($request);
    }
}
