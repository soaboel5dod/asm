<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\OfficeActivityRequest;
use App\Http\Resources\LocationTrackerResource;
use App\Http\Resources\OfficeActivityResource;
use App\LocationTracker;
use App\OfficeActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfficeActivitiesController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/office_activities",
     *   tags={"Office Activities"},
     *   operationId="office_activities",
     *   summary="get all Office Activity",
     *  @SWG\Parameter(
     *    name="location_tracker_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location Tracker id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $officeActivities = OfficeActivity::where('location_tracker_id', $request->get('location_tracker_id'))->get();
        $locationTracker = LocationTracker::find($request->get('location_tracker_id'));
        return response()->json([
            'locationTracker' => new LocationTrackerResource($locationTracker),
            'officeActivities' => OfficeActivityResource::collection($officeActivities)
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/office_activities_history",
     *   tags={"Office Activities"},
     *   operationId="office_activities_history",
     *   summary="get Office Activity History",
     *  @SWG\Parameter(
     *    name="location_tracker_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location Tracker id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request)
    {
        $officeActivities = OfficeActivity::where('location_tracker_id', $request->get('location_tracker_id'))->get();
        $locationTracker = LocationTracker::find($request->get('location_tracker_id'));

        return response()->json([
            'locationTracker' => new LocationTrackerResource($locationTracker),
            'officeActivities' => OfficeActivityResource::collection($officeActivities)
        ]);
    }

    /**
     * @SWG\Post(
     *   path="/office_activities",
     *   tags={"Office Activities"},
     *   operationId="office_activities_store",
     *   summary="Add New Office Activity",
     *   @SWG\Parameter(
     *    name="activity_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Activity id",
     *  ),
     *  @SWG\Parameter(
     *    name="location_tracker_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location Tracker id",
     *  ),
     *  @SWG\Parameter(
     *    name="start",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Start Time",
     *  ),
     *  @SWG\Parameter(
     *    name="note",
     *    in= "formData",
     *    type="string",
     *    description="Note",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param OfficeActivityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OfficeActivityRequest $request)
    {
        $officeActivity = OfficeActivity::create($request->input());
        return response()->json([
            'officeActivity' => $officeActivity,
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/office_activities/{office_activity}",
     *   tags={"Office Activities"},
     *   operationId="office_activity_show",
     *   summary="Show Office Activity",
     *  @SWG\Parameter(
     *    name="office_activity",
     *    in="path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Office Activity id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param OfficeActivity $office_activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(OfficeActivity $office_activity)
    {
        return response()->json([
            'office_activity' => $office_activity
        ]);
    }

    /**
     * @SWG\Patch(
     *   path="/office_activities/{office_activity}",
     *   tags={"Office Activities"},
     *   operationId="office_activities_update",
     *   summary="Update Activity",
     *   @SWG\Parameter(
     *    name="office_activity",
     *    in= "path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Office Activity id",
     *  ),
     *   @SWG\Parameter(
     *    name="note",
     *    in= "formData",
     *    type="string",
     *    description="Note",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @param OfficeActivity $office_activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, OfficeActivity $office_activity)
    {
        $office_activity->update(['note' => $request->get('note')]);
        return response()->json([
            'office_activity' => $office_activity,
        ]);
    }

    /**
     * @SWG\Post(
     *   path="/office_activities/complete",
     *   tags={"Office Activities"},
     *   operationId="office_activities_complete",
     *   summary="Complete Office Activity",
     *   @SWG\Parameter(
     *    name="id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Office Activity id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete(Request $request)
    {
        $officeActivity = OfficeActivity::find($request->get('id'));
        $officeActivity->update(['end' => Carbon::now()]);

        return response()->json([
            'officeActivity' => $officeActivity,
        ]);
    }

    /**
     * @SWG\Delete(
     *   path="/office_activities/{office_activity}",
     *   tags={"Office Activities"},
     *   operationId="office_activities_delete",
     *   summary="Delete Activity",
     *   @SWG\Parameter(
     *    name="office_activity",
     *    in= "path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Office Activity id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param OfficeActivity $office_activity
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(OfficeActivity $office_activity)
    {
        $office_activity->delete();
        
        return response()->json([
            'Message' => 'Activity deleted Successfully !', 200]);
    }

    /**
     * @SWG\Post(
     *   path="/upload_attachment/{office_activity}",
     *   tags={"Office Activities"},
     *   operationId="upload_attachment",
     *   summary="Upload Attachment",
     *   @SWG\Parameter(
     *    name="office_activity",
     *    in= "path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Office Activity id",
     *  ),
     *   @SWG\Parameter(
     *    name="images",
     *    in= "formData",
     *    required=true,
     *    description="Attachments",
     *    type="file",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @param OfficeActivity $office_activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAttachment(Request $request, OfficeActivity $office_activity)
    {
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $office_activity->addImage($image->store('office_activities', 'public'));
            }
        }
        return response()->json([
            'attachments' => $office_activity->photos,
        ]);
    }
    /**
     * @SWG\Post(
     *   path="/office_activities/break",
     *   tags={"Office Activities"},
     *   operationId="break",
     *   summary="Break",
     *   @SWG\Parameter(
     *    name="location_tracker_id",
     *    in= "formData",
     *    required=true,
     *    description="Location Track id",
     *    type="integer",
     *    format="int",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function break(Request $request)
    {
        $location_tracker = LocationTracker::where(['id'=>$request->input('location_tracker_id'), 'location_id' => 1,
            'active' => 1])
            ->get();
        if (count($location_tracker)) {
            $office_activity = OfficeActivity::create($request->input() +
                ['start' => Carbon::now() , 'activity_id' => 1, 'break' => 1]);
            return response()->json([
                'office_activity' => $office_activity,
            ]);
        } else {
            return response()->json([
                'message' => 'You Have to be in Office to Take Break',
            ]);
        }
    }

    /**
     * @SWG\Post(
     *   path="/office_activities/end_break/{office_activity}",
     *   tags={"Office Activities"},
     *   operationId="endBreak",
     *   summary="End Break",
     *   @SWG\Parameter(
     *    name="office_activity",
     *    in= "path",
     *    required=true,
     *    description="Break id",
     *    type="integer",
     *    format="int",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param OfficeActivity $office_activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function endBreak(OfficeActivity $office_activity)
    {
        $office_activity->update(['end' => Carbon::now()]);

        return response()->json([
            'office_activity' => $office_activity,
        ]);
    }
}
