<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="127.0.0.1:8000",
 *     basePath="/api",
 *     @SWG\Info(
 *         version="3.0",
 *         title="ASM API",
 *         description="This is a sample server for ASM API",
 *         @SWG\Contact(
 *             email="info@asm.com"
 *         ),
 *     ),
 * )
 */

class APIController extends Controller
{
    protected function responseSuccess($message = '')
    {
        return $this->response(true, $message);
    }

    protected function responseFail($message = '')
    {
        return $this->response(false, $message);
    }

    protected function response($status = false, $message = '')
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
        ]);
    }
}
