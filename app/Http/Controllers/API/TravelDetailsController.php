<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\TravelDetailRequest;
use App\TravelDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TravelDetailsController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/travel_details",
     *   tags={"Travel Details"},
     *   operationId="travel_details_store",
     *   summary="Add New Travel Details",
     *  @SWG\Parameter(
     *    name="location_tracker_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location Tracker id",
     *  ),
     *  @SWG\Parameter(
     *    name="type",
     *    in= "formData",
     *    enum={"local", "domestic", "international"},
     *    required=true,
     *    type="string",
     *    description="Type",
     *  ),
     *  @SWG\Parameter(
     *    name="start_location",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Start Location",
     *  ),
     *  @SWG\Parameter(
     *    name="destination_location",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Destination Location",
     *  ),
     *  @SWG\Parameter(
     *    name="start_time",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Start Time",
     *  ),
     *  @SWG\Parameter(
     *    name="end_time",
     *    in= "formData",
     *    type="string",
     *    description="End Time",
     *  ),
     *  @SWG\Parameter(
     *    name="note",
     *    in= "formData",
     *    type="string",
     *    description="Note",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param TravelDetailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TravelDetailRequest $request)
    {
        $travelDetail = TravelDetail::create($request->input());
        return response()->json([
            'travelDetail' => $travelDetail,
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/travel_details/{travel_detail}",
     *   tags={"Travel Details"},
     *   operationId="travel_details_show",
     *   summary="Show Travel Details",
     *  @SWG\Parameter(
     *    name="travel_detail",
     *    in="path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Travel Detail id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param TravelDetail $travel_detail
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(TravelDetail $travel_detail)
    {
        return response()->json([
            'travel_detail' => $travel_detail
        ]);
    }

    /**
     * @SWG\Patch(
     *   path="/travel_details/{travel_detail}",
     *   tags={"Travel Details"},
     *   operationId="travel_detail_update",
     *   summary="Update Travel Detail",
     *   @SWG\Parameter(
     *    name="travel_detail",
     *    in= "path",
     *    required=true,
     *    type="string",
     *    description="Travel Detail id",
     *  ),
     *  @SWG\Parameter(
     *    name="start_location",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Start Location",
     *  ),
     *  @SWG\Parameter(
     *    name="destination_location",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Destination Location",
     *  ),
     *  @SWG\Parameter(
     *    name="start_time",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Start Time",
     *  ),
     *  @SWG\Parameter(
     *    name="end_time",
     *    in= "formData",
     *    type="string",
     *    description="End Time",
     *  ),
     *   @SWG\Parameter(
     *    name="note",
     *    in= "formData",
     *    type="string",
     *    description="Note",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param TravelDetailRequest $request
     * @param TravelDetail $travel_detail
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TravelDetailRequest $request, TravelDetail $travel_detail)
    {
        $travel_detail->update($request->input());
        return response()->json([
            'travel_detail' => $travel_detail,
        ]);
    }
}
