<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\VisitActivityRequest;
use App\VisitActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitActivitiesController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/visit_activities",
     *   tags={"Visit Activities"},
     *   operationId="visit_activities_store",
     *   summary="Add New Visit Activity",
     *  @SWG\Parameter(
     *    name="visit_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Visit id",
     *  ),
     *   @SWG\Parameter(
     *    name="activity_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Activity id",
     *  ),
     *  @SWG\Parameter(
     *    name="contact_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Type",
     *  ),
     *  @SWG\Parameter(
     *    name="note",
     *    in= "formData",
     *    type="string",
     *    description="Note",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param VisitActivityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VisitActivityRequest $request)
    {
        $visit_activity = VisitActivity::create($request->input());
        return response()->json([
            'visit_activity' => $visit_activity,
        ]);
    }
}
