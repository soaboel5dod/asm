<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\TypeCheckRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeCheckController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/type_check",
     *   tags={"typeCheck"},
     *   operationId="type_check",
     *   summary="Check User type sales or management",
     *   @SWG\Parameter(
     *    name="user_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *   @SWG\Parameter(
     *    name="type",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="sales or management",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param TypeCheckRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TypeCheckRequest $request)
    {
        $user = User::where(['id' => $request->get('user_id'), 'type' => $request->get('type')])->first();

        if (count($user)) {
            return response()->json([
                'user' => $user
            ]);
        } else {
            return response()->json([
                'Message' => 'Access Denied !'
            ]);
        }
    }
}
