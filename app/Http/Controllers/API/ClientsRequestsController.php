<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;

class ClientsRequestsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/requests",
     *   tags={"Clients Requests"},
     *   operationId="clients_request",
     *   summary="get All clients requests",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clients = Client::where('approved', false)->get();
        return response()->json([
            'clients' => $clients->load('user')
        ]);
    }

    /**
     * @SWG\Post(
     *   path="/requests",
     *   tags={"Clients Requests"},
     *   operationId="create_client_request",
     *   summary="Request to Add new Client",
     *  @SWG\Parameter(
     *    name="name",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Client Name",
     *  ),
     *  @SWG\Parameter(
     *    name="user_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User ID",
     *  ),
     *  @SWG\Parameter(
     *    name="location",
     *    in= "formData",
     *    type="string",
     *    description="Location",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Client::create($request->input());
        return response()->json([
            'message' => 'Request Send Successfully',
        ]);
    }

    /**
     * @SWG\Patch(
     *   path="/requests/{id}",
     *   tags={"Clients Requests"},
     *   operationId="approve_client",
     *   summary="Approve Client",
     *   @SWG\Parameter(
     *    name="id",
     *    in= "path",
     *    required=true,
     *    type="integer",
     *    description="Client id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $client->approved = 1;
        $client->save();
        return response()->json([
            'client' => $client,
        ]);
    }
}
