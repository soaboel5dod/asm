<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/managers",
     *   tags={"Managers"},
     *   operationId="managers",
     *   summary="get All Managers",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function managers()
    {
        $managers = User::where(['type'=> 'management', 'admin' => 0])->get(['id', 'name']);
        return response()->json([
            'managers' => $managers
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/sales",
     *   tags={"sales"},
     *   operationId="sales",
     *   summary="get All sales Users For this Manager",
     *  @SWG\Parameter(
     *    name="manager_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Manager id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sales(Request $request)
    {
        $sales = User::where(['type'=> 'sales', 'admin' => 0, 'manager_id' => $request->input('manager_id')])
            ->get(['id', 'name']);
        return response()->json([
            'sales' => $sales
        ]);
    }

}
