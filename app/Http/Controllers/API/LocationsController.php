<?php

namespace App\Http\Controllers\API;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/locations",
     *   tags={"Locations"},
     *   operationId="locations",
     *   summary="Locations",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $locations = Location::get();
        return response()->json([
            'locations' => $locations
        ]);
    }
}
