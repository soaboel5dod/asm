<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\ResetRequest;
use App\Http\Controllers\Controller;
use App\Reset;

class ResetsController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/resets",
     *   tags={"Reset Password Request"},
     *   operationId="reset_password",
     *   summary="Reset Password",
     *   @SWG\Parameter(
     *    name="email",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="User Email",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param ResetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ResetRequest $request)
    {
        Reset::create($request->input());

        return response()->json([
            'message' => 'Request Send Successfully !'
        ]);
    }
}
