<?php

namespace App\Http\Controllers\API;

use App\Contact;
use App\Http\Requests\API\ContactRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/contacts",
     *   tags={"Contacts"},
     *   operationId="contacts",
     *   summary="get All Contacts",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clients = Contact::get();
        $clients->load('client');
        return response()->json([
            'clients' => $clients
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/user_contacts",
     *   tags={"Contacts"},
     *   operationId="user_contacts",
     *   summary="get All User Contacts",
     *  @SWG\Parameter(
     *    name="user_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="user id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userContacts(Request $request)
    {
        $clients = Contact::where('user_id', $request->input('user_id'))->get();
        $clients->load('client');
        return response()->json([
            'clients' => $clients
        ]);
    }

    /**
     * @SWG\Post(
     *   path="/contacts",
     *   tags={"Contacts"},
     *   operationId="contacts_create",
     *   summary="Add New Contact",
     *   @SWG\Parameter(
     *    name="client_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Client id",
     *  ),
     *   @SWG\Parameter(
     *    name="user_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *  @SWG\Parameter(
     *    name="name",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="Contact Name",
     *  ),
     *  @SWG\Parameter(
     *    name="location",
     *    in= "formData",
     *    type="string",
     *    description="Location",
     *  ),
     *  @SWG\Parameter(
     *    name="position",
     *    in= "formData",
     *    type="string",
     *    description="Position",
     *  ),
     *  @SWG\Parameter(
     *    name="mobile_1",
     *    in= "formData",
     *    type="string",
     *    description="Mobile 1",
     *  ),
     *  @SWG\Parameter(
     *    name="mobile_2",
     *    in= "formData",
     *    type="string",
     *    description="Mobile 2",
     *  ),
     *  @SWG\Parameter(
     *    name="email_1",
     *    in= "formData",
     *    type="string",
     *    description="Email 1",
     *  ),
     *  @SWG\Parameter(
     *    name="email_2",
     *    in= "formData",
     *    type="string",
     *    description="Email 2",
     *  ),
     *  @SWG\Parameter(
     *    name="religion",
     *    in= "formData",
     *    type="string",
     *    description="Religion",
     *  ),
     *  @SWG\Parameter(
     *    name="department",
     *    in= "formData",
     *    type="string",
     *    description="Department",
     *  ),
     *   @SWG\Parameter(
     *    name="image",
     *    in= "formData",
     *    required=false,
     *    description="Business Card",
     *    type="file",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param ContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ContactRequest $request)
    {
        $contact = Contact::create($request->input());
        if ($request->hasFile('image')) {
            $contact->addImage($request->file('image')->store('contacts', 'public'), true);
        }
        return response()->json([
            'contact' => $contact->load('photo'),
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/contacts/{contact}",
     *   tags={"Contacts"},
     *   operationId="contact_data",
     *   summary="Show Contact data",
     *  @SWG\Parameter(
     *    name="contact",
     *    in="path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Contact id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Contact $contact)
    {
        return response()->json([
            'contact' => $contact->load('photo', 'client')
        ]);
    }
}
