<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/clients",
     *   tags={"Clients"},
     *   operationId="clients",
     *   summary="get All clients",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clients = Client::get(['id', 'name', 'location']);
        return response()->json([
            'clients' => $clients
        ]);
    }
}
