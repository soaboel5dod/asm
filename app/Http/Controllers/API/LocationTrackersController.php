<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\LocationTrackerRequest;
use App\Http\Resources\LocationTrackerResource;
use App\Location;
use App\LocationTracker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LocationTrackersController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/location_trackers",
     *   tags={"Location Trackers"},
     *   operationId="location_trackers",
     *   summary="Track User Location",
     *   @SWG\Parameter(
     *    name="user_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *  @SWG\Parameter(
     *    name="location_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location id",
     *  ),
     *   @SWG\Parameter(
     *    name="day_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Day id",
     *  ),
     *   @SWG\Parameter(
     *    name="lat",
     *    in= "formData",
     *    type="string",
     *    description="Lat",
     *  ),
     *   @SWG\Parameter(
     *    name="long",
     *    in= "formData",
     *    type="string",
     *    description="long",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param LocationTrackerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LocationTrackerRequest $request)
    {
        $location = Location::where(['id' => $request->input('location_id'), 'type' => 'office'])->first();
        if ($location) {
            $distance = $this->getDistance($request->input('lat'), $request->input('long'), $location->id);
            if ($distance->distance <= 0.400) {
                $locationTracker = LocationTracker::create($request->input() + ['start' => Carbon::now()]);
                return response()->json([
                    'locationTracker' => $locationTracker,
                ]);
            } else {
                return response()->json([
                    'message' => 'You have to be In 400 m range',
                ]);
            }
        }

        $locationTracker = LocationTracker::create($request->input() + ['start' => Carbon::now()]);
        return response()->json([
            'locationTracker' => $locationTracker,
        ]);
    }

    /**
     * @SWG\Patch(
     *   path="/location_trackers/{location_tracker}",
     *   tags={"Location Trackers"},
     *   operationId="location_tracker_update",
     *   summary="Check out",
     *   @SWG\Parameter(
     *    name="location_tracker",
     *    in= "path",
     *    required=true,
     *    type="string",
     *    description="Location Tracker id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param LocationTracker $location_tracker
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LocationTracker $location_tracker)
    {
        $location_tracker->update(['checkout' => Carbon::now(), 'active' => 0]);
        return response()->json([
            'locationTracker' => new LocationTrackerResource($location_tracker),
        ]);
    }

    public function getDistance($lat, $lng, $id)
    {
        $results =  Location::select(
            DB::raw("*,
                      ( 6371 * acos( cos( radians(" . $lat . ") ) *
                        cos( radians( latitude ) )
                        * cos( radians( longitude ) - radians(" . $lng . ")
                        ) + sin( radians(" .$lat. ") ) *
                        sin( radians( latitude ) ) )
                      ) AS distance"))
            ->where('id', $id)
            ->orderBy('distance', 'asc')
            ->first();
        return $results;
    }
}
