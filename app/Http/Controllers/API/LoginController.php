<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/login",
     *   tags={"login"},
     *   operationId="login",
     *   summary="login",
     *   @SWG\Parameter(
     *    name="email",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    description="User Email",
     *  ),
     *   @SWG\Parameter(
     *    name="password",
     *    in= "formData",
     *    required=true,
     *    type="string",
     *    format="password",
     *    description="User Password",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $success =  $user->createToken('MyApp')->accessToken;
            return response()->json(['user' => $user, 'token' => $success], 200);
        } else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
