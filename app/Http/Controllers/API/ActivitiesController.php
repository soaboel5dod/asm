<?php

namespace App\Http\Controllers\API;

use App\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivitiesController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/activities",
     *   tags={"Activities"},
     *   operationId="activities",
     *   summary="get All Activities",
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $activities = Activity::where('id', '!=', 1)->get(['id', 'name']);
        return response()->json([
            'activities' => $activities
        ]);
    }
}
