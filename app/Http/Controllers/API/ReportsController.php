<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\Day;
use App\Http\Resources\VisitResource;
use App\LocationTracker;
use App\User;
use App\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/reports",
     *   tags={"Reports"},
     *   operationId="reports",
     *   summary="View Report",
     *  @SWG\Parameter(
     *    name="user_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *  @SWG\Parameter(
     *    name="manager_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Manager id",
     *  ),
     *  @SWG\Parameter(
     *    name="from_date",
     *    in="query",
     *    required=true,
     *    type="string",
     *    description="yyyy-mm-dd",
     *  ),
     *  @SWG\Parameter(
     *    name="to_date",
     *    in="query",
     *    required=true,
     *    type="string",
     *    description="yyyy-mm-dd",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $totalWorkingHours = 0;
        $totalOfficeHours = 0;
        $totalVisitsHours =0;
        $totalTravelHours = 0;
        $from = $request->input('from_date');
        $to = $request->input('to_date');
        $visits = VisitResource::collection(Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
          ->withCount('visitActivities')->get());
        $totalVisits = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get()->count();

        $existingVisits = VisitResource::collection(Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', false)->get());
        $newVisits = VisitResource::collection(Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', true)->get());

        $existingVisitsCount = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', false)->get()->count();
        $newVisitsCount = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', true)->get()->count();
        $workingDays = Day::where('user_id', $request->input('user_id'))
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get()->count();
        $locationTrackers = LocationTracker::where('user_id', $request->input('user_id'))
            ->where('checkout', '!=', null)
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get();
        $clientsCount = Client::where('user_id', $request->input('user_id'))
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get()->count();
        foreach ($locationTrackers as $locationTracker) {
            $totalWorkingHours+= $locationTracker['checkout']->diffInHours($locationTracker['start']);
            if ($locationTracker->location_id == 1) {
                $totalOfficeHours+= $locationTracker['checkout']->diffInHours($locationTracker['start']);
            } elseif ($locationTracker->location_id == 2) {
                $totalVisitsHours+=$locationTracker['checkout']->diffInHours($locationTracker['start']);
            } elseif ($locationTracker->location_id == 3) {
                $totalTravelHours+=$locationTracker['checkout']->diffInHours($locationTracker['start']);
            }
        }
        $f_visits = array();
        foreach ($visits as $key => $visit) {
            $date = $visit->created_at->toDateString();
            $f_visits[$date][$key] = $visit;
        }
        $e_visits = array();
        foreach ($existingVisits as $key => $visit) {
            $date = $visit->created_at->toDateString();
            $e_visits[$date][$key] = $visit;
        }
        $n_visits = array();
        foreach ($newVisits as $key => $visit) {
            $date = $visit->created_at->toDateString();
            $n_visits[$date][$key] = $visit;
        }
        return response()->json([
            'visits' => $f_visits,
            'newVisits' => $n_visits,
            'existingVisits' => $e_visits,
            'newVisitsCount' => $newVisitsCount,
            'existingVisitsCount' => $existingVisitsCount,
            'totalVisits' => $totalVisits,
            'workingDays' => $workingDays,
            'totalWorkingHours' => $totalWorkingHours,
            'totalOfficeHours' => $totalOfficeHours,
            'totalVisitsHours' => $totalVisitsHours,
            'totalTravelHours' => $totalTravelHours,
            'clientsCount' => $clientsCount
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/reports/export",
     *   tags={"Reports"},
     *   operationId="reports_export",
     *   summary="Report export",
     *  @SWG\Parameter(
     *    name="user_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *  @SWG\Parameter(
     *    name="manager_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Manager id",
     *  ),
     *  @SWG\Parameter(
     *    name="from_date",
     *    in="query",
     *    required=true,
     *    type="string",
     *    description="yyyy-mm-dd",
     *  ),
     *  @SWG\Parameter(
     *    name="to_date",
     *    in="query",
     *    required=true,
     *    type="string",
     *    description="yyyy-mm-dd",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function export(Request $request)
    {
        $totalWorkingHours = 0;
        $totalOfficeHours = 0;
        $totalVisitsHours =0;
        $totalTravelHours = 0;
        $from = $request->input('from_date');
        $to = $request->input('to_date');

        $existingVisitsCount = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', false)->get()->count();
        $newVisitsCount = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where('user_id', $request->input('user_id'));
        })->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where('new', true)->get()->count();
        $workingDays = Day::where('user_id', $request->input('user_id'))
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get()->count();
        $locationTrackers = LocationTracker::where('user_id', $request->input('user_id'))
            ->where('checkout', '!=', null)
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get();
        $clientsCount = Client::where('user_id', $request->input('user_id'))
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->get()->count();
        foreach ($locationTrackers as $locationTracker) {
            $totalWorkingHours+= $locationTracker['checkout']->diffInHours($locationTracker['start']);
            if ($locationTracker->location_id == 1) {
                $totalOfficeHours+= $locationTracker['checkout']->diffInHours($locationTracker['start']);
            } elseif ($locationTracker->location_id == 2) {
                $totalVisitsHours+=$locationTracker['checkout']->diffInHours($locationTracker['start']);
            } elseif ($locationTracker->location_id == 3) {
                $totalTravelHours+=$locationTracker['checkout']->diffInHours($locationTracker['start']);
            }
        }
        // Initialize the array which will be passed into the Excel
        // generator.
        $reportArray = [];

        // Define the Excel spreadsheet headers
        $reportArray[] = ['Existing Visits', 'New Visits','Total Visits','New Client'
        ,'Total Office Hours', 'Total Visits Hours', 'Total Travel Hours','Total Working Hours', 'Working Days'];
        $reportArray[] = [$existingVisitsCount, $newVisitsCount, $existingVisitsCount + $newVisitsCount, $clientsCount
            ,$totalOfficeHours, $totalVisitsHours, $totalTravelHours,$totalWorkingHours, $workingDays];
        $user = User::find($request->input('user_id'));
        $title = $user->name .'Report from '. $from . 'to' .$to;
        $data = Excel::create($title, function ($excel) use ($reportArray, $title, $request) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle($title);
            $excel->setCreator('Manger');
            $excel->setDescription('Report For'. $request->input('date'));
            // Build the spreadsheet, passing in the report array
            $excel->sheet('sheet1', function ($sheet) use ($reportArray) {
                $sheet->fromArray($reportArray, null, 'A1', false, false);
            });
        })->store('xls', storage_path('app/public/'), true);

        return response()->json([
           'file_path' => url('/public/storage/'.$title.'.xls')
        ]);
    }

}
