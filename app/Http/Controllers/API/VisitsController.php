<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\VisitRequest;
use App\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\VisitResource;
use Illuminate\Support\Facades\DB;

class VisitsController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/visits",
     *   tags={"Visits"},
     *   operationId="visits",
     *   summary="get all Visits",
     *  @SWG\Parameter(
     *    name="user_id",
     *    in="query",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $f_visits = array();
        $visits = Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where(['user_id'=> $request->input('user_id'), 'active' => 0]);
        })->with('locationTracker', 'client')->withCount('visitActivities')->orderBy('created_at', 'desc')->get();

        if (count($visits)) {
            $visits = VisitResource::collection($visits);
            foreach ($visits as $key => $visit) {
                $date = $visit->created_at->toDateString();
                $f_visits[$date][$key] = $visit;
            }
        }
        $current_visit =Visit::whereHas('locationTracker', function ($query) use ($request) {
            $query->where(['user_id'=> $request->input('user_id'), 'active' => 1]);
        })->with('locationTracker', 'client')->withCount('visitActivities')->first();
        if (count($current_visit)) {
            $current_visit = new VisitResource($current_visit);
        }
        return response()->json(['current_visit' => $current_visit, 'visits' => $f_visits], 200);
    }

    /**
     * @SWG\Post(
     *   path="/visits",
     *   tags={"Visits"},
     *   operationId="visits_store",
     *   summary="Add New Visit",
     *  @SWG\Parameter(
     *    name="location_tracker_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Location Tracker id",
     *  ),
     *   @SWG\Parameter(
     *    name="client_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Client id",
     *  ),
     *  @SWG\Parameter(
     *    name="new",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    description="0 Or 1",
     *  ),
     *  @SWG\Parameter(
     *    name="type",
     *    in= "formData",
     *    enum={"new_opportunity", "follow_up"},
     *    required=true,
     *    type="string",
     *    description="Type",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param VisitRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VisitRequest $request)
    {
        $visit = Visit::create($request->input());
        return response()->json([
            'visit' => $visit->load('client'),
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/visits/{visit}",
     *   tags={"Visits"},
     *   operationId="visit_show",
     *   summary="Show Visit",
     *  @SWG\Parameter(
     *    name="visit",
     *    in="path",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="Visit id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Visit $visit
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Visit $visit)
    {
        return response()->json([
            'visit' => new VisitResource($visit)
        ]);
    }
}
