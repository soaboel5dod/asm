<?php

namespace App\Http\Controllers\API;

use App\Day;
use App\Http\Requests\API\DayRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DaysController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/days",
     *   tags={"Days"},
     *   operationId="day",
     *   summary="Start Day",
     *   @SWG\Parameter(
     *    name="user_id",
     *    in= "formData",
     *    required=true,
     *    type="integer",
     *    format="int",
     *    description="User id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param DayRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DayRequest $request)
    {
        $day = Day::create($request->input() + ['start' => Carbon::now()->toDateTimeString()]);
        return response()->json([
            'day' => $day,
        ]);
    }

    /**
     * @SWG\Patch(
     *   path="/days/{day}",
     *   tags={"Days"},
     *   operationId="day_end",
     *   summary="End Day",
     *   @SWG\Parameter(
     *    name="day",
     *    in= "path",
     *    required=true,
     *    type="string",
     *    description="Day id",
     *  ),
     *   @SWG\Response(
     *    response=200,
     *    description="success",
     *   ),
     *   @SWG\Response(
     *    response=400,
     *    description="error",
     *   ),
     *  )
     * @param Day $day
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Day $day)
    {
        $day->update(['end' => Carbon::now()->toDateTimeString()]);
        return response()->json([
            'day' => $day,
        ]);
    }
}
