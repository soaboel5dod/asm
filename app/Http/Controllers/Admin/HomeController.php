<?php

namespace App\Http\Controllers\Admin;

use App\Reset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $resets = Reset::count();

        return view('admin.index', compact('resets'));
    }
}
