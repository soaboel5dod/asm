<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfficeActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'activity' => $this->activity->name,
            'start' => $this->start->format('h:i A'),
            'end' => $this->end ? $this->end->format('H:i A') : '',
            'note' => $this->note,
            'break' => $this->break,
            'photos' => $this->photos
        ];
    }
}
