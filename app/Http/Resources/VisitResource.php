<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VisitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'client' => $this->client->name,
            'new' => $this->new ? 'New' : 'Exist',
            'type' => $this->type,
            'start' => $this->locationTracker->start->format('h:i A'),
            'checkout' => $this->locationTracker->checkout ? $this->locationTracker->checkout->format('H:i A') : '',
            'date' => $this->created_at->toFormattedDateString(),
            'activities_count' => $this->visitActivities()->count(),
            'activities' => VisitActivityResource::collection($this->visitActivities)
        ];
    }
}
