<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationTrackerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'location' => $this->location->name,
            'start' => $this->start->toDayDateTimeString(),
            'checkout' => $this->checkout ? $this->checkout->toDayDateTimeString() : '',
        ];
    }
}
