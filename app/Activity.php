<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['name'];

    public function officeActivities()
    {
        return $this->hasMany(OfficeActivity::class);
    }

    public function visitActivities()
    {
        return $this->hasMany(VisitActivity::class);
    }
}
