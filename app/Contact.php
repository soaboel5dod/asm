<?php

namespace App;

use App\Traits\Photoable;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use Photoable;
    protected $fillable = ['name', 'position', 'mobile_1', 'mobile_2',
        'email_1', 'email_2', 'religion', 'department', 'client_id', 'user_id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
