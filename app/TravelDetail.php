<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelDetail extends Model
{
    protected $fillable = ['location_tracker_id', 'type', 'start_location', 'destination_location',
        'start_time', 'end_time', 'note'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function locationTracker()
    {
        return $this->belongsTo(LocationTracker::class);
    }


}
