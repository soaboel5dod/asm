<?php
use Illuminate\Support\Facades\Request;

/**
 * Checks if the specified route is active.
 *
 * @param $route
 * @return bool
 */
function isRouteActive($route)
{
    return !! preg_match("/^{$route}$/", Request::route()->getName());
}

/**
 * @param $route
 * @return string
 */
function activateRouteClass($route)
{
    if (! isRouteActive($route)) {
        return '';
    }

    return 'class=active';
}

/**
 * @return string
 */
function switchLocaleUrl()
{
    $locale = config('app.locale');
    if ($locale === 'en') {
        return '/ar/' . trim(Request::path(), '/');
    }

    return '/' . collect(Request::segments())->splice(1)->implode('/');
}

/**
 * Retrieves a value from cache, if not found runs the callable.
 *
 * @param $key
 * @param $callable
 * @return mixed
 * @throws Exception
 */
function fromCacheOrSave($key, $callable)
{
    $value = cache($key);
    if (! $value) {
        if (! is_callable($callable)) {
            // not very intuitive
            throw new \Exception('$callable is not callable.');
        }

        $value = $callable();
        cache()->forever($key, $value);
    }

    return $value;
}